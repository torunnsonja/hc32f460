/**
 * @file    gpio.c
 * @brief   管脚配置
 * @note
 * @note    PH2     LED_RED
 * @note    PC13    LED_GREEN
 * @note
 * @note    PC0     KEY1
 * @note    PC1     KEY2
 * @note    PC2     KEY3
 * @note    PC3     KEY4
 * @note
 * @note    PA4     OLED_I2C_SCL
 * @note    PA5     OLED_I2C_SDA
 * @note    PA6     OLED_NRST
 *
 * @note    PB6     SWD_I
 * @note    PB5     EN_SWD_I
 * @note    PB4     SWD_O
 * @note    PD2     EN_SWD_O
 * @note    PC12    EN_SWD_CLK
 * @note    PC11    SWD_SWCLK
 * @note    PC10    SWD_SWO
 * @note    PA15    SWD_RST
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "gpio.h"

/* Extern variables ----------------------------------------------------------*/

/* Private includes ----------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private constants ---------------------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

/**
 * @brief  I2C初始化
 * @param  None
 * @retval 初始化结果
 */
static en_result_t i2c1_init(void)
{
    en_result_t enRet;
    stc_i2c_init_t stcI2cInit;
    stc_clk_freq_t stcClkFreq;

    I2C_DeInit(M4_I2C1);

    /* Get system clock frequency */
    CLK_GetClockFreq(&stcClkFreq);

    MEM_ZERO_STRUCT(stcI2cInit);
    stcI2cInit.u32Pclk3 = stcClkFreq.pclk3Freq;
    stcI2cInit.u32Baudrate = 400000ul;
    stcI2cInit.u32SclTime = 0ul;
    enRet = I2C_Init(M4_I2C1, &stcI2cInit);

    I2C_BusWaitCmd(M4_I2C1, Enable);

    return enRet;
}

/**
 * @brief  四线SPI初始化
 * @param  None
 * @retval 初始化结果
 */
static en_result_t qspi_init(void)
{
    en_result_t enRet;
    stc_qspi_init_t stcQspiInit;

    /* configuration structure initialization */
    MEM_ZERO_STRUCT(stcQspiInit);

    /* Configuration peripheral clock */
    PWC_Fcg1PeriphClockCmd(PWC_FCG1_PERIPH_QSPI, Enable);

    /* Configuration QSPI pin */
    PORT_SetFunc(QSCK_PORT, QSCK_PIN, Func_Qspi, Disable);
    PORT_SetFunc(QSIO0_PORT, QSIO0_PIN, Func_Qspi, Disable);
    PORT_SetFunc(QSIO1_PORT, QSIO1_PIN, Func_Qspi, Disable);
    PORT_SetFunc(QSIO2_PORT, QSIO2_PIN, Func_Qspi, Disable);
    PORT_SetFunc(QSIO3_PORT, QSIO3_PIN, Func_Qspi, Disable);
    PORT_SetFunc(QSSN_PORT, QSSN_PIN, Func_Qspi, Disable);

    /* Configuration QSPI structure */
    stcQspiInit.enClkDiv = QspiHclkDiv2;                                         // QspiHclkDiv4 时钟分频
    stcQspiInit.enSpiMode = QspiSpiMode0;                                        // QspiSpiMode0 SPI模式-模式3
    stcQspiInit.enBusCommMode = QspiBusModeRomAccess;                            // QspiBusModeRomAccess
    stcQspiInit.enPrefetchMode = QspiPrefetchStopComplete;                       // QspiPrefetchStopComplete
    stcQspiInit.enPrefetchFuncEn = Disable;                                      // Disable 预读取功能--无效
    stcQspiInit.enQssnValidExtendTime = QspiQssnValidExtendNot;                  // QspiQssnValidExtendSck32 CS延长32个QSCK周期
    stcQspiInit.enQssnIntervalTime = QspiQssnIntervalQsck8;                      // QspiQssnIntervalQsck8 CS最小无效时间8个QSCK周期
    stcQspiInit.enQsckDutyCorr = QspiQsckDutyCorrNot;                            // QspiQsckDutyCorrHalfHclk 上升沿滞后半个周期
    stcQspiInit.enVirtualPeriod = QspiVirtualPeriodQsck8;                        // QspiVirtualPeriodQsck6 虚拟周期6个QSCK
    stcQspiInit.enWpPinLevel = QspiWpPinOutputLow;                               // QspiWpPinOutputHigh 写保护电平高电平
    stcQspiInit.enQssnSetupDelayTime = QspiQssnSetupDelayHalfQsck;               // QspiQssnSetupDelay1Dot5Qsck 提前1.5个周期输出CS
    stcQspiInit.enQssnHoldDelayTime = QspiQssnHoldDelayHalfQsck;                 // QspiQssnHoldDelay1Dot5Qsck 滞后1.5个周期释放CS
    stcQspiInit.enFourByteAddrReadEn = Disable;                                  // Disable 不使用4字节地址读指令
    stcQspiInit.enAddrWidth = QspiAddressByteThree;                              // QspiAddressByteThree 地址宽度3字节
    stcQspiInit.stcCommProtocol.enReadMode = QspiReadModeFourWiresIO;            // QspiReadModeFourWiresIO 读取模式选择4线输入输出快速读
    stcQspiInit.stcCommProtocol.enTransInstrProtocol = QspiProtocolFourWiresSpi; // QspiProtocolFourWiresSpi 指令协议模式-扩展式
    stcQspiInit.stcCommProtocol.enTransAddrProtocol = QspiProtocolFourWiresSpi;  // QspiProtocolFourWiresSpi 地址协议模式--扩展式
    stcQspiInit.stcCommProtocol.enReceProtocol = QspiProtocolFourWiresSpi;       // QspiProtocolFourWiresSpi 数据接收协议模式--扩展式
    stcQspiInit.u8RomAccessInstr = QSPI_3BINSTR_FOUR_WIRES_IO_READ;              // QSPI_3BINSTR_FOUR_WIRES_IO_READ 指令代码
    enRet = QSPI_Init(&stcQspiInit);

    return enRet;
}

/**
 * @brief  管教配置初始化
 * @param  None
 * @retval None
 */
void gpio_init(void)
{
    stc_port_init_t stcPortInit;

    MEM_ZERO_STRUCT(stcPortInit);
    stcPortInit.enPinMode = Pin_Mode_Out;

    PORT_Init(LED_RED_PORT, LED_RED_PIN, &stcPortInit);
    PORT_Init(LED_GREEN_PORT, LED_GREEN_PIN, &stcPortInit);

    PORT_Init(OLED_NRST_PORT, OLED_NRST_PIN, &stcPortInit);
    PORT_ResetPortData(OLED_NRST_PORT, OLED_NRST_PIN);

    PORT_Init(EN_SWD_I_PORT, EN_SWD_I_PIN, &stcPortInit);
    PORT_Init(SWD_O_PORT, SWD_O_PIN, &stcPortInit);
    PORT_Init(EN_SWD_O_PORT, EN_SWD_O_PIN, &stcPortInit);
    PORT_Init(EN_SWD_CLK_PORT, EN_SWD_CLK_PIN, &stcPortInit);
    PORT_Init(SWD_SWCLK_PORT, SWD_SWCLK_PIN, &stcPortInit);
    PORT_Init(SWD_SWO_PORT, SWD_SWO_PIN, &stcPortInit);
    PORT_Init(SWD_RST_PORT, SWD_RST_PIN, &stcPortInit);

    stcPortInit.enPinMode = Pin_Mode_In;
    PORT_Init(KEY_1_PORT, KEY_1_PIN, &stcPortInit);
    PORT_Init(KEY_2_PORT, KEY_2_PIN, &stcPortInit);
    PORT_Init(KEY_3_PORT, KEY_3_PIN, &stcPortInit);
    PORT_Init(KEY_4_PORT, KEY_4_PIN, &stcPortInit);

    PORT_Init(SWD_I_PORT, SWD_I_PIN, &stcPortInit);

    /* Initialize I2C port*/
    PORT_SetFunc(OLED_I2C_SCL_PORT, OLED_I2C_SCL_PIN, Func_I2c1_Scl, Disable);
    PORT_SetFunc(OLED_I2C_SDA_PORT, OLED_I2C_SDA_PIN, Func_I2c1_Sda, Disable);

    /* Enable I2C Peripheral*/
    PWC_Fcg1PeriphClockCmd(PWC_FCG1_PERIPH_I2C1, Enable);

    /* Conf I2C */
    if (i2c1_init() != Ok) {
    }

    /* Conf QSPI */
    if (qspi_init() != Ok) {
    }
    return;
}
