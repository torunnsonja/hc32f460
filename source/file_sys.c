/**
 * @file    file_sys.c
 * @brief   文件系统
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "ff.h"
#include "file_sys.h"

/* Extern variables ----------------------------------------------------------*/

/* Private includes ----------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private constants ---------------------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

/**
 * @brief  文件系统初始化
 * @param  None
 * @retval 初始化结果
 */
uint8_t file_sys_init(void)
{
    FATFS fs;
    FRESULT res;
    BYTE work[FF_MAX_SS];
    const MKFS_PARM opt = {FM_FAT, 0, 0, 0, 0};

    res = f_mount(&fs, "", 0);
    if (res != FR_OK) {
        res = f_mkfs("", &opt, work, sizeof(work));
        if (res != FR_OK) {
            return 1;
        }
        res = f_mount(&fs, "", 0);
        if (res != FR_OK) {
            return 2;
        }
    }
    return 0;
}

/**
 * @brief  文件系统测试
 * @param  None
 * @retval 测试结果 0 通过
 */
uint8_t file_sysy_test(void)
{
    FIL fp;
    FRESULT res;
    DIR dj;
    FILINFO fno; 
    uint8_t buffer[16];
    uint32_t deal_num = 0;

    res = f_open(&fp, "/hello.text", FA_OPEN_APPEND | FA_WRITE);
    if (res != FR_OK) {
        return 1;
    }

    for(uint8_t i=0; i<ARRAY_LEN(buffer); ++i) {
        buffer[i] = i;
    }
    /* write test */
    res = f_write(&fp, buffer, sizeof(buffer), &deal_num);
    if (res != FR_OK) {
        return 2;
    }

    if (deal_num != sizeof(buffer)) {
        return 3;
    }

    /* read test */
    memset(buffer, 0, sizeof(buffer));
    res = f_read(&fp, buffer, sizeof(buffer), &deal_num);
    if (res != FR_OK) {
        return 4;
    }

    if (deal_num != sizeof(buffer)) {
        return 5;
    }

    for (uint8_t i=0; i<ARRAY_LEN(buffer); ++i) {
        if (buffer[i] != i) {
            return 6;
        }
    }

    /* find test */
    deal_num = 0;
    res = f_findfirst(&dj, &fno, "/", "*.txt");
    while (res == FR_OK && fno.fname[0]) {
        if (fno.fname) {
            deal_num += 1;
        }
        res = f_findnext(&dj, &fno);
    }
    if (!deal_num) {
        return 7;
    }

    return 0;
}
