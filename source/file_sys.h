/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FILE_SYS_H
#define __FILE_SYS_H

/* Includes ------------------------------------------------------------------*/

/* Private includes ----------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
uint8_t file_sys_init(void);
uint8_t file_sysy_test(void);

/* Private defines -----------------------------------------------------------*/

#endif
