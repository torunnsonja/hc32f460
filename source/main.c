/**
 *******************************************************************************
 * @file  main.c
 * @brief Main program template.
 @verbatim
   Change Logs:
   Date             Author          Notes
   2020-06-30       Zhangxl         First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2016, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software is owned and published by:
 * Huada Semiconductor Co., Ltd. ("HDSC").
 *
 * BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND
 * BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
 *
 * This software contains source code for use with HDSC
 * components. This software is licensed by HDSC to be adapted only
 * for use in systems utilizing HDSC components. HDSC shall not be
 * responsible for misuse or illegal use of this software for devices not
 * supported herein. HDSC is providing this software "AS IS" and will
 * not be responsible for issues arising from incorrect user implementation
 * of the software.
 *
 * Disclaimer:
 * HDSC MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
 * REGARDING THE SOFTWARE (INCLUDING ANY ACCOMPANYING WRITTEN MATERIALS),
 * ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING,
 * WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED
 * WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED
 * WARRANTY OF NONINFRINGEMENT.
 * HDSC SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT,
 * NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT
 * LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION,
 * LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR
 * INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA,
 * SAVINGS OR PROFITS,
 * EVEN IF Disclaimer HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
 * INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED
 * FROM, THE SOFTWARE.
 *
 * This software may be replicated in part or whole for the licensed use,
 * with the restriction that this Disclaimer and Copyright notice must be
 * included with each copy of this software, whether used in part or whole,
 * at all times.
 *******************************************************************************
 */

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include "main.h"
#include "gpio.h"
#include "w25qxx.h"
#include "file_sys.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usb_bsp.h"

/*******************************************************************************
 * Local type definitions ('typedef')
 ******************************************************************************/

/*******************************************************************************
 * Local pre-processor symbols/macros ('#define')
 ******************************************************************************/

/*******************************************************************************
 * Global variable definitions (declared in header file with 'extern')
 ******************************************************************************/

/*******************************************************************************
 * Local function prototypes ('static')
 ******************************************************************************/

/*******************************************************************************
 * Local variable definitions ('static')
 ******************************************************************************/
/**
 * @brief ICG parameters configuration
 */
/* The ICG area is filled with F by default, HRC = 16MHZ,
   Please modify this value as required */
#if defined(__GNUC__) && !defined(__CC_ARM) /* GNU Compiler */
const uint32_t u32ICG[] __attribute__((section(".icg_sec"))) =
#elif defined(__CC_ARM)
const uint32_t u32ICG[] __attribute__((at(0x400))) =
#elif defined(__ICCARM__)
__root const uint32_t u32ICG[] @0x400 =
#else
#error "unsupported compiler!!"
#endif
    {
        /* ICG 0~ 3 */
        0xFFFFFFFFUL,
        0xFFFFFFFFUL,
        0xFFFFFFFFUL,
        0xFFFFFFFFUL,
        /* ICG 4~ 7 */
        0xFFFFFFFFUL,
        0xFFFFFFFFUL,
        0xFFFFFFFFUL,
        0xFFFFFFFFUL,
    };

/*******************************************************************************
 * Function implementation - global ('extern') and local ('static')
 ******************************************************************************/
USB_OTG_CORE_HANDLE USB_OTG_dev;

/**
 *******************************************************************************
 ** \brief System clock init function
 **
 ** \param [in] None
 **
 ** \retval None
 **
 ******************************************************************************/
static void SystemClk_Init(void)
{
    stc_clk_sysclk_cfg_t stcSysClkCfg;
    stc_clk_xtal_cfg_t stcXtalCfg;
    stc_clk_mpll_cfg_t stcMpllCfg;
    stc_clk_upll_cfg_t stcUpllCfg;

    MEM_ZERO_STRUCT(stcSysClkCfg);
    MEM_ZERO_STRUCT(stcXtalCfg);
    MEM_ZERO_STRUCT(stcMpllCfg);
    MEM_ZERO_STRUCT(stcUpllCfg);

    /* Set bus clk div. */
    stcSysClkCfg.enHclkDiv = ClkSysclkDiv1;  // 168MHz
    stcSysClkCfg.enExclkDiv = ClkSysclkDiv2; // 84MHz
    stcSysClkCfg.enPclk0Div = ClkSysclkDiv1; // 168MHz
    stcSysClkCfg.enPclk1Div = ClkSysclkDiv2; // 84MHz
    stcSysClkCfg.enPclk2Div = ClkSysclkDiv4; // 42MHz
    stcSysClkCfg.enPclk3Div = ClkSysclkDiv4; // 42MHz
    stcSysClkCfg.enPclk4Div = ClkSysclkDiv2; // 84MHz
    CLK_SysClkConfig(&stcSysClkCfg);

    /* Switch system clock source to MPLL. */
    /* Use Xtal as MPLL source. */
    stcXtalCfg.enMode = ClkXtalModeOsc;
    stcXtalCfg.enDrv = ClkXtalLowDrv;
    stcXtalCfg.enFastStartup = Enable;
    CLK_XtalConfig(&stcXtalCfg);
    CLK_XtalCmd(Enable);

    /* MPLL config. (Xtal / pllmDiv * plln) */
    stcMpllCfg.pllmDiv = 1u;
    stcMpllCfg.plln = 28u;   /* 12 / 1 * 28 = 336 */
    stcMpllCfg.PllpDiv = 4u; // MPLLP = 84
    stcMpllCfg.PllqDiv = 2u;
    stcMpllCfg.PllrDiv = 7u; // MPLLR 168/4 = 48
    CLK_SetPllSource(ClkPllSrcXTAL);
    CLK_MpllConfig(&stcMpllCfg);

    /* other sources setting */
    CLK_SetPeriClkSource(ClkPeriSrcMpllq);

    /* flash read wait cycle setting */
    EFM_Unlock();
    EFM_SetLatency(EFM_LATENCY_4);
    EFM_Lock();

    /* Enable MPLL. */
    CLK_MpllCmd(Enable);

    /* Wait MPLL ready. */
    while (Set != CLK_GetFlagStatus(ClkFlagMPLLRdy)) {
    }

    /* Switch system clock source to MPLL. */
    CLK_SetSysClkSource(CLKSysSrcMPLL);

    stcUpllCfg.pllmDiv = 2u;
    stcUpllCfg.plln = 84u;
    stcUpllCfg.PllpDiv = 7u; // 48M
    stcUpllCfg.PllqDiv = 7u;
    stcUpllCfg.PllrDiv = 7u;
    CLK_UpllConfig(&stcUpllCfg);
    CLK_UpllCmd(Enable);
    /* Wait UPLL ready. */
    while (Set != CLK_GetFlagStatus(ClkFlagUPLLRdy)) {
        ;
    }

    /* Set USB clock source */
    CLK_SetUsbClkSource(ClkUsbSrcUpllp);
}

/**
 * @brief  Main function of template project
 * @param  None
 * @retval int32_t return value, if needed
 */
int32_t main(void)
{
    stc_clk_freq_t Clkdata;

    PORT_DebugPortSetting(0x1C, Disable);
    CLK_HrcCmd(Enable);
    CLK_SetSysClkSource(ClkSysSrcHRC);
    Ddl_Delay1ms(2000);
    SystemClk_Init();
    CLK_LrcCmd(Disable);
    CLK_Xtal32Cmd(Disable);
    CLK_GetClockFreq(&Clkdata);
    SysTick_Config(Clkdata.hclkFreq / 1000);
    NVIC_EnableIRQ(SysTick_IRQn);
    Ddl_UartInit();

    gpio_init();
    W25QXX_Init();
    file_sys_init();
    file_sysy_test();

    USBD_Init(&USB_OTG_dev, USB_OTG_FS_CORE_ID, &USR_desc, &USBD_MSC_cb, &USR_cb);

    /* add your code here */
    while (1) {
    }
}

/*******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/
