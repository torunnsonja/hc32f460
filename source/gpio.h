/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GPIO_H
#define __GPIO_H

/* Includes ------------------------------------------------------------------*/

/* Private includes ----------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/
/* LED */
#define LED_RED_PORT (PortH)
#define LED_RED_PIN (Pin02)

#define LED_GREEN_PORT (PortC)
#define LED_GREEN_PIN (Pin13)

/* KEY */
#define KEY_1_PORT (PortC)
#define KEY_1_PIN (Pin00)

#define KEY_2_PORT (PortC)
#define KEY_2_PIN (Pin01)

#define KEY_3_PORT (PortC)
#define KEY_3_PIN (Pin02)

#define KEY_4_PORT (PortC)
#define KEY_4_PIN (Pin03)

/* OLED I2C */
#define OLED_I2C_SCL_PORT (PortA)
#define OLED_I2C_SCL_PIN (Pin04)

#define OLED_I2C_SDA_PORT (PortA)
#define OLED_I2C_SDA_PIN (Pin05)

#define OLED_NRST_PORT (PortA)
#define OLED_NRST_PIN (Pin06)

/* EX SWD */
#define SWD_I_PORT (PortB)
#define SWD_I_PIN (Pin06)

#define EN_SWD_I_PORT (PortB)
#define EN_SWD_I_PIN (Pin05)

#define SWD_O_PORT (PortB)
#define SWD_O_PIN (Pin04)

#define EN_SWD_O_PORT (PortD)
#define EN_SWD_O_PIN (Pin02)

#define EN_SWD_CLK_PORT (PortC)
#define EN_SWD_CLK_PIN (Pin12)

#define SWD_SWCLK_PORT (PortC)
#define SWD_SWCLK_PIN (Pin11)

#define SWD_SWO_PORT (PortC)
#define SWD_SWO_PIN (Pin10)

#define SWD_RST_PORT (PortA)
#define SWD_RST_PIN (Pin15)

/* W25Q64 QSPI */
#define QSCK_PORT (PortB)
#define QSCK_PIN (Pin14)

#define QSIO0_PORT (PortB)
#define QSIO0_PIN (Pin13)

#define QSIO1_PORT (PortB)
#define QSIO1_PIN (Pin12)

#define QSIO2_PORT (PortB)
#define QSIO2_PIN (Pin10)

#define QSIO3_PORT (PortB)
#define QSIO3_PIN (Pin02) 

#define QSSN_PORT (PortB)
#define QSSN_PIN (Pin01) 




/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
void gpio_init(void);

/* Private defines -----------------------------------------------------------*/

#endif
