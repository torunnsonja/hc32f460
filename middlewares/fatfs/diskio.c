/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"     /* Obtains integer types */
#include "diskio.h" /* Declarations of disk functions */
#include "w25qxx.h"

/* Definitions of physical drive number for each drive */
#define SPI_FLASH 0

#define FLASH_SECTOR_SIZE 512
#define FLASH_BLOCK_SIZE 8          /* 8 * 512 = 4k */
WORD FLASH_SECTOR_COUNT = 2048 * 8; /* 8M */

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(BYTE pdrv /* Physical drive nmuber to identify the drive */
)
{
    DSTATUS stat;

    switch (pdrv) {
        case SPI_FLASH:
            stat = RES_OK;
            return stat;
    }
    return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(BYTE pdrv /* Physical drive nmuber to identify the drive */
)
{
    DSTATUS stat;

    switch (pdrv) {
        case SPI_FLASH:
            if (W25QXX_TYPE != W25Q64) {
                W25QXX_Init();
            }
            stat = (W25QXX_TYPE == W25Q64) ? (RES_OK) : (RES_NOTRDY);
            return stat;
    }
    return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(BYTE pdrv,    /* Physical drive nmuber to identify the drive */
                  BYTE * buff,  /* Data buffer to store read data */
                  LBA_t sector, /* Start sector in LBA */
                  UINT count    /* Number of sectors to read */
)
{
    DRESULT res;

    if (!count) {
        return RES_PARERR;
    }

    switch (pdrv) {
        case SPI_FLASH:
            for (; count > 0; count--) {
                W25QXX_Read(buff, sector * FLASH_SECTOR_SIZE, FLASH_SECTOR_SIZE);
                sector++;
                buff += FLASH_SECTOR_SIZE;
            }
            res = RES_OK;
            return res;
    }

    return RES_PARERR;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write(BYTE pdrv,         /* Physical drive nmuber to identify the drive */
                   const BYTE * buff, /* Data to be written */
                   LBA_t sector,      /* Start sector in LBA */
                   UINT count         /* Number of sectors to write */
)
{
    DRESULT res;

    if (!count) {
        return RES_PARERR;
    }
    switch (pdrv) {
        case SPI_FLASH:
            for (; count > 0; count--) {
                W25QXX_Write((uint8_t *)buff, sector * FLASH_SECTOR_SIZE, FLASH_SECTOR_SIZE);
                sector++;
                buff += FLASH_SECTOR_SIZE;
            }
            res = RES_OK;
            return res;
    }

    return RES_PARERR;
}

#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl(BYTE pdrv,  /* Physical drive nmuber (0..) */
                   BYTE cmd,   /* Control code */
                   void * buff /* Buffer to send/receive control data */
)
{
    DRESULT res;

    switch (pdrv) {
        case SPI_FLASH:
            switch (cmd) {
                case CTRL_SYNC:
                    res = RES_OK;
                    break;
                case GET_SECTOR_SIZE:
                    *(WORD *)buff = FLASH_SECTOR_SIZE;
                    res = RES_OK;
                    break;
                case GET_BLOCK_SIZE:
                    *(WORD *)buff = FLASH_BLOCK_SIZE;
                    res = RES_OK;
                    break;
                case GET_SECTOR_COUNT:
                    *(DWORD *)buff = FLASH_SECTOR_COUNT;
                    res = RES_OK;
                    break;
                default:
                    res = RES_PARERR;
                    break;
            }
            return res;
    }
    return RES_PARERR;
}
